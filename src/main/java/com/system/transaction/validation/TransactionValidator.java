package com.system.transaction.validation;

import com.system.transaction.domain.Record;

public class TransactionValidator extends Validator{
	
	public static boolean hasInvalidEndBalance(Record record) {
		if(record!=null) {
			return record.getEndBalance().compareTo(record.getStartBalance().add(record.getMutation()))==0?false:true;
		}
		return false;
		
		
	}

}
