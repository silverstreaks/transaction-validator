package com.system.transaction.validation;

import org.apache.commons.lang3.math.NumberUtils;

public class Validator {

	public static boolean isNumberic(String str) {
		return NumberUtils.isNumber(str);
	}
}
