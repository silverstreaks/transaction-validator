package com.system.transaction.exception;

public class IncompatibileFileTypeException extends Exception {
	public IncompatibileFileTypeException() {
	}
	public IncompatibileFileTypeException(String message) {
		super(message);
	}
	
}
