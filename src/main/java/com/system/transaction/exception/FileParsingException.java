package com.system.transaction.exception;

public class FileParsingException extends Exception {

	public FileParsingException() {
	}
	public FileParsingException(String message) {
		super(message);
	}
}
