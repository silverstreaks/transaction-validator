package com.system.transaction.parser;

import java.io.File;

@FunctionalInterface
public interface Parse {
	void parseFile(File file);
}
