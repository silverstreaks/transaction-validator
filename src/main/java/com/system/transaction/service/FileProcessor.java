package com.system.transaction.service;

import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.system.transaction.domain.Record;
import com.system.transaction.exception.FileParsingException;
import com.system.transaction.exception.IncompatibileFileTypeException;
import com.system.transaction.parser.Parse;

@Component("fileProcessor")
public class FileProcessor {

	private File file;
	
	private Set<Record> recordList;
	private List<Record> errorList;
	
	
	public void process() throws IncompatibileFileTypeException,FileParsingException{
		if ("xml".equals(FilenameUtils.getExtension(file.getName()))) {
			parseXMLFile();
		} else if ("csv".equals(FilenameUtils.getExtension(file.getName()))) {
			parseCSVFile();
		} else {
			throw new IncompatibileFileTypeException();
		}
	}
	
	private void parseCSVFile() {
		
		Parse parser = (csvFile) -> {
			
			try {
				CSVParser csvParser = new CSVParser(new FileReader(csvFile), CSVFormat.DEFAULT);
				recordList = new HashSet<Record>();
				errorList = new ArrayList<Record>();
				List<CSVRecord> csvRecords = csvParser.getRecords();
				
				int rowIndex = 0;
				for (CSVRecord csvRecord : csvRecords) {
			     			    
					if(rowIndex++>0) {
				    	Record record = new Record();
				    	int columnIndex =0 ;
						record.setReferenceNumber(csvRecord.get(columnIndex++));
						record.setAccountNumber(csvRecord.get(columnIndex++));
						record.setDescription(csvRecord.get(columnIndex++));
						record.setStartBalance(new BigDecimal(csvRecord.get(columnIndex++)));
						record.setMutation(new BigDecimal(csvRecord.get(columnIndex++)));
						record.setEndBalance(new BigDecimal(csvRecord.get(columnIndex++)));
						
						if(!recordList.add(record)) {
							errorList.add(record);
						}
					}
			    }
				csvParser.close();
			}catch(Exception ex) {
			}
		};
		parser.parseFile(file);
	}


	private void parseXMLFile(){
		Parse parser = (xmlFile) -> {
			try {

				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(xmlFile);

				doc.getDocumentElement().normalize();

				NodeList nList = doc.getElementsByTagName("record");


				recordList = new HashSet<Record>();
				errorList = new ArrayList<Record>();

				for (int index = 0; index < nList.getLength(); index++) {
					Node nNode = nList.item(index);

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;

						Record record = new Record();
						record.setReferenceNumber(eElement.getAttribute("reference"));
						record.setAccountNumber(
								eElement.getElementsByTagName("accountNumber").item(0).getTextContent());
						record.setDescription(eElement.getElementsByTagName("description").item(0).getTextContent());
						record.setStartBalance(
								new BigDecimal(eElement.getElementsByTagName("startBalance").item(0).getTextContent()));
						record.setMutation(
								new BigDecimal(eElement.getElementsByTagName("mutation").item(0).getTextContent()));
						record.setEndBalance(
								new BigDecimal(eElement.getElementsByTagName("endBalance").item(0).getTextContent()));

						if(!recordList.add(record)) {
							errorList.add(record);
						}

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		};

		parser.parseFile(file);

	}

	public File getFile() {
		return file;
	}
	
	public void setFile(File file) {
		this.file = file;
	}

	public List<Record> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<Record> errorList) {
		this.errorList = errorList;
	}

	public Set<Record> getRecordList() {
		return recordList;
	}

	public void setRecordList(Set<Record> recordList) {
		this.recordList = recordList;
	}

}
