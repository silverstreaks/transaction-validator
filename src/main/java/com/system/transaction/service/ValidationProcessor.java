package com.system.transaction.service;

import com.system.transaction.exception.ValidationException;
import static com.system.transaction.validation.TransactionValidator.hasInvalidEndBalance;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.system.transaction.domain.Record;

@Component("validationProcessor")
public class ValidationProcessor{
	
	private Set<Record> recordList;
	private List<Record> errorList; 
	
	
	public void process() throws ValidationException{
		if(recordList == null) {
			throw new ValidationException();
		}
		
		recordList
		.stream()
		.filter(key -> hasInvalidEndBalance(key))
		.forEach(errorList::add);
	}


	public List<Record> getErrorList() {
		return errorList;
	}


	public void setErrorList(List<Record> errorList) {
		this.errorList = errorList;
	}


	public Set<Record> getRecordList() {
		return recordList;
	}


	public void setRecordList(Set<Record> recordList) {
		this.recordList = recordList;
	}

}
