package com.system.transaction.service;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.system.transaction.domain.Record;
import com.system.transaction.exception.FileParsingException;
import com.system.transaction.exception.IncompatibileFileTypeException;
import com.system.transaction.exception.ValidationException;

@Component("report")
public class ReportProcessor {
	
	@Autowired
	private FileProcessor fileProcessor;
	
	@Autowired
	private ValidationProcessor validationProcessor;
	
	
	public List<Record> getInvalidTransactions(File file) throws IncompatibileFileTypeException{
		
		if(file== null) {
			throw new IncompatibileFileTypeException();
		}
		try {
			doFileParsing(file);
		}catch(FileParsingException | ValidationException ex) {
			return null;
		}catch(IncompatibileFileTypeException ic) {
			throw ic;
		}

		try {
			doTransactionValidation();
		} catch (ValidationException e) {
			return null;
		}
		return validationProcessor.getErrorList();
		
	}

	private void doTransactionValidation() throws ValidationException {
		validationProcessor.setRecordList(fileProcessor.getRecordList());
		validationProcessor.setErrorList(fileProcessor.getErrorList());
		validationProcessor.process();
	}

	private void doFileParsing(File file) throws IncompatibileFileTypeException, FileParsingException, ValidationException {
		fileProcessor.setFile(file);
		fileProcessor.process();
	}

	public FileProcessor getFileProcessor() {
		return fileProcessor;
	}

	public void setFileProcessor(FileProcessor fileProcessor) {
		this.fileProcessor = fileProcessor;
	}

	public ValidationProcessor getValidationProcessor() {
		return validationProcessor;
	}

	public void setValidationProcessor(ValidationProcessor validationProcessor) {
		this.validationProcessor = validationProcessor;
	}
	
}
