package com.system.transaction.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;

import com.system.transaction.domain.Record;
import com.system.transaction.exception.IncompatibileFileTypeException;
import com.system.transaction.file.FileUtils;
import com.system.transaction.service.ReportProcessor;


@Controller
public class ReportValidationController {
	
	private boolean hasErrors;
	
	@Autowired
	private ReportProcessor report;

	@RequestMapping(value="/")
	public ModelAndView test(HttpServletResponse response) throws IOException{
		return new ModelAndView("home");
	}
	
	@RequestMapping(value = "/parse", method = RequestMethod.POST)
	public String processFile(@RequestParam("file") MultipartFile file, ModelMap modelMap) {
		List<Record> errorList;
		try {
			errorList = report.getInvalidTransactions(FileUtils.convert(file));
			if(errorList !=null && errorList.size()>0) {
				modelMap.addAttribute("errors",errorList);
			}else {
				hasErrors = true;
				modelMap.addAttribute("message","File processing completed sucessfully, No errors found");
			}
		} catch (IOException e) {
			hasErrors = true;
			modelMap.addAttribute("message", "No file selected");
		} catch(IncompatibileFileTypeException ex) {
			hasErrors = true;
			modelMap.addAttribute("message", "Incompatible file type");
		}
	    return "home";
	}
	
	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver multipartResolver() {
	    CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
	    multipartResolver.setMaxUploadSize(100000);
	    return multipartResolver;
	}

	public boolean hasErrors() {
		return hasErrors;
	}

	public ReportProcessor getReport() {
		return report;
	}

	public void setReport(ReportProcessor report) {
		this.report = report;
	}
	
}
