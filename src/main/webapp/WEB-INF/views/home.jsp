<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
    <head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"/> -->	
    	<title>Transaction Validator</title>
    	<script type="application/javascript">
    	
</script>
  </head>
  
  <body>
 
 <div class="container">
 	<form method="POST" action="/transaction-validator/parse" enctype="multipart/form-data">
		<h2> Rabobank Report Validaton
		</h2>
		
		<div class="input-group">
			<div class="custom-file">
				<input type="file" class="custom-file-input" id="inputGroupFile04" name="file"
					aria-describedby="inputGroupFileAddon04"> <label
					class="custom-file-label" for="inputGroupFile04">Choose
					file</label>
				</input>	
			</div>
			<div class="input-group-append">
				<input type="submit" class="btn btn-outline-secondary" name="submit"
					id="inputGroupFileAddon04"></button>
			</div>
		</div>
		
	</form>
	
	 <c:if test="${not empty message}">
		<div class="panel panel-default">
    		<div class="panel-body text-center alert alert-danger ">
    		<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
    		${message}</div>
  		</div>
  	</c:if>
        
        <c:if test="${not empty errors}">

        <table class="table table-striped">
  		<thead>
		    <tr>
		      <th scope="col">Reference Number</th>
		      <th scope="col">Account Number</th>
		      <th scope="col">Description</th>
		      <th scope="col">Start Balance</th>
		      <th scope="col">Mutation</th>
		      <th scope="col">End Balance</th>
		    </tr>
  		</thead>
		  <tbody>
		  <c:forEach var="error" items="${errors}">
		    <tr>
		      <td>${error.referenceNumber}</td>
		      <td>${error.accountNumber}</td>
		      <td>${error.description}</td>
		      <td>${error.startBalance}</td>
		      <td>${error.mutation}</td>
		      <td>${error.endBalance}</td>
		    </tr>
		    </c:forEach>
		  </tbody>
		 </table>
		 </c:if>
		 </div>
		       
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    	<script>
    	$(document).ready(function(){
    		$('input[type="file"]').change(function(e){
    			console.log(e);
        		var fileName = e.target.files[0].name;
        		console.log(fileName);
       			$('.custom-file-label').html(fileName);
    		});
    	});
    	</script>
    </body>
</html>
