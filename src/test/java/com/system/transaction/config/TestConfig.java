package com.system.transaction.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = "com.system.transaction.test")
public class TestConfig {
}
