package com.system.transaction.test.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.system.transaction.config.TestConfig;
import com.system.transaction.domain.Record;
import com.system.transaction.exception.FileParsingException;
import com.system.transaction.exception.IncompatibileFileTypeException;
import com.system.transaction.service.FileProcessor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = { TestConfig.class })
public class FileProcessorTest {
	
	private File xmlFile;
	private File xmlFileWithoutData;
	private File csvFile;
	private File csvFileWithoutData;

	@Before
	public void setup() {
		try {
		ClassLoader classLoader = getClass().getClassLoader();
		xmlFile = new File(classLoader.getResource("records.xml").getFile());
		csvFile = new File(classLoader.getResource("records.csv").getFile());
		xmlFileWithoutData = new File(classLoader.getResource("records_without_data.xml").getFile());
		csvFileWithoutData = new File(classLoader.getResource("records_without_data.csv").getFile());
		}catch(Exception e) {
		}
	}
	
	@Test
	public void xml_file_with_data() {
		FileProcessor processor = new FileProcessor();
		try {
			processor.setFile(xmlFile);
			processor.process();
		} catch (IncompatibileFileTypeException | FileParsingException e) {
		}
		List<Record> error = processor.getErrorList();
		Set<Record> record = processor.getRecordList();
		assertNotNull(error);
		assertTrue(error.size()==0);
		assertTrue(record.size()>0);
	}
	
	@Test
	public void xml_file_without_data() {
		FileProcessor processor = new FileProcessor();
		try {
			processor.setFile(xmlFileWithoutData);
			processor.process();
		} catch (IncompatibileFileTypeException | FileParsingException e) {
		}
		List<Record> error = processor.getErrorList();
		Set<Record> record = processor.getRecordList();
		assertNull(error);
		assertNull(record);
	}
	
	@Test
	public void csv_file_with_data() {
		FileProcessor processor = new FileProcessor();
		try {
			processor.setFile(csvFile);
			processor.process();
		} catch (IncompatibileFileTypeException | FileParsingException e) {
		}
		List<Record> error = processor.getErrorList();
		Set<Record> record = processor.getRecordList();
		assertNotNull(error);
		assertTrue(error.size()==0);
		assertTrue(record.size()>0);
	}
	
	@Test
	public void csv_file_without_data() {
		FileProcessor processor = new FileProcessor();
		try {
			processor.setFile(csvFileWithoutData);
			processor.process();
		} catch (IncompatibileFileTypeException | FileParsingException e) {
		}
		List<Record> error = processor.getErrorList();
		Set<Record> record = processor.getRecordList();
		assertNotNull(error);
		assertTrue(error.size()==0);
		assertTrue(record.size()==0);
	}
}
