package com.system.transaction.test.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.system.transaction.config.TestConfig;
import com.system.transaction.domain.Record;
import com.system.transaction.exception.IncompatibileFileTypeException;
import com.system.transaction.service.FileProcessor;
import com.system.transaction.service.ReportProcessor;
import com.system.transaction.service.ValidationProcessor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = { TestConfig.class })
public class ReportTest {
	
	private ReportProcessor report;
	private File xmlFile;
	private File xmlFileWithoutData;
	
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		ClassLoader classLoader = getClass().getClassLoader();
		xmlFile = new File(classLoader.getResource("records.xml").getFile());
		xmlFileWithoutData = new File(classLoader.getResource("records_without_data.xml").getFile());
		report = new ReportProcessor();
		
	}
	
	@Test
	public void process_report_successfully() {
		report.setFileProcessor(new FileProcessor());
		report.setValidationProcessor(new ValidationProcessor());
		
		List<Record> errorList = null;
		try {
			errorList = report.getInvalidTransactions(xmlFile);
		} catch (IncompatibileFileTypeException e) {
		}
		
		assertNotNull(errorList);
		assertTrue(errorList.size()>0);
	}
	
	@Test
	public void process_report_with_null_file() {
		report.setFileProcessor(new FileProcessor());
		report.setValidationProcessor(new ValidationProcessor());
		
		List<Record> errorList = null;
		try {
			errorList = report.getInvalidTransactions(null);
		} catch (IncompatibileFileTypeException e) {
		}
		
		assertNull(errorList);
	}
	
	@Test
	public void process_report_with_exception() {
		
		List<Record> errorList=null;
		try {
			report.setFileProcessor(new FileProcessor());
			report.setValidationProcessor(new ValidationProcessor());
			errorList = report.getInvalidTransactions(xmlFileWithoutData);
		} catch (IncompatibileFileTypeException e) {
		}
		
		assertNull(errorList);
	}
	

}
