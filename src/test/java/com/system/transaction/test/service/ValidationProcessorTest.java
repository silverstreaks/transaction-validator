package com.system.transaction.test.service;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.system.transaction.config.TestConfig;
import com.system.transaction.domain.Record;
import com.system.transaction.exception.ValidationException;
import com.system.transaction.service.ValidationProcessor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = { TestConfig.class })
public class ValidationProcessorTest {
	
	@Mock
	private ValidationProcessor validationProcessor;
	private Set<Record> mockRecordList;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		mockRecordList = createMockRecordList();
	}
	
	@Test
	public void validate_list() {
		validationProcessor = new ValidationProcessor();
		validationProcessor.setRecordList(mockRecordList);
		validationProcessor.setErrorList(new ArrayList<Record>());
		try {
			validationProcessor.process();
		} catch (ValidationException e) {
		}
		assertTrue(validationProcessor.getErrorList().size()>0);
		
	}
	
	@Test
	public void validate_empty_list() {
		validationProcessor = new ValidationProcessor();
		validationProcessor.setRecordList(new HashSet());
		validationProcessor.setErrorList(new ArrayList<Record>());
		try {
			validationProcessor.process();
		} catch (ValidationException e) {
		}
		assertTrue(validationProcessor.getErrorList().size()==0);
		
	}

	private Set<Record> createMockRecordList() {
		 Set<Record> mockRecordList = new HashSet<Record>();
			Record record = new Record();
			record.setAccountNumber("123456");
			record.setStartBalance(new BigDecimal("25.1"));
			record.setMutation(new BigDecimal("-2"));
			record.setEndBalance(new BigDecimal("23.1"));
			
			mockRecordList.add(record);
			record = new Record();
			record.setAccountNumber("23425343");
			record.setStartBalance(new BigDecimal("1.5"));
			record.setMutation(new BigDecimal("1.5"));
			record.setEndBalance(new BigDecimal("4"));
			
			mockRecordList.add(record);
			
		return mockRecordList;
	}
}
