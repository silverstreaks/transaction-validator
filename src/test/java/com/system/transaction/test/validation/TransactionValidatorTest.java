package com.system.transaction.test.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.system.transaction.config.TestConfig;
import com.system.transaction.domain.Record;
import com.system.transaction.validation.TransactionValidator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = { TestConfig.class })
public class TransactionValidatorTest {
	
	@Test
	public void validate_correct_record() {
		Record record = new Record();
		record.setStartBalance(new BigDecimal("23.5"));
		record.setMutation(new BigDecimal("-0.5"));
		record.setEndBalance(new BigDecimal("23.0"));
		
		assertFalse(TransactionValidator.hasInvalidEndBalance(record));
		
	}
	
	@Test
	public void validate_incorrect_record() {
		Record record = new Record();
		record.setStartBalance(new BigDecimal("23.5"));
		record.setMutation(new BigDecimal("0.5"));
		record.setEndBalance(new BigDecimal("23"));
		
		assertTrue(TransactionValidator.hasInvalidEndBalance(record));
		
	}
	
}
