package com.system.transaction.test.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.system.transaction.config.TestConfig;
import com.system.transaction.validation.Validator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = { TestConfig.class })
public class ValidatorTest {

	@Test
	public void test_is_number() {
		assertTrue(Validator.isNumberic("5"));
	}
	
	@Test
	public void test_is_not_number() {
		assertFalse(Validator.isNumberic("a"));
	}
	
	@Test
	public void test_white_space() {
		assertFalse(Validator.isNumberic(""));
	}
}
