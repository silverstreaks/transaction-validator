package com.system.transaction.test.controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.ui.ModelMap;

import com.system.transaction.config.TestConfig;
import com.system.transaction.controller.ReportValidationController;
import com.system.transaction.domain.Record;
import com.system.transaction.exception.IncompatibileFileTypeException;
import com.system.transaction.file.FileUtils;
import com.system.transaction.service.ReportProcessor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = { TestConfig.class })
public class ReportValidationControllerTest {

	@Mock
	private ReportProcessor mockReportProcessor;
	
	@Mock
	private FileUtils fileUtils;
	private ReportValidationController controller;
	
	private List<Record> errorList;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		
		errorList = new ArrayList<Record>();
		errorList.add(new Record());
		errorList.add(new Record());
	}
	
	@Test
	public void test_processing_successful() {
		controller = new ReportValidationController();
		MockMultipartFile someFile = new MockMultipartFile("data", "filename.xml", "application/xml",
				"some xml".getBytes());
		
		try {
			Mockito.when(mockReportProcessor.getInvalidTransactions(Mockito.anyObject())).thenReturn(errorList);
		} catch (IncompatibileFileTypeException e) {
		}
		controller.setReport(mockReportProcessor);
		controller.processFile(someFile, new ModelMap());
		assertFalse(controller.hasErrors());
	}
	
	@Test
	public void test_process_failed_with_null_response() {
		controller = new ReportValidationController();
		MockMultipartFile someFile = new MockMultipartFile("data", "filename.xml", "application/xml",
				"some xml".getBytes());
		
		try {
			Mockito.when(mockReportProcessor.getInvalidTransactions(Mockito.anyObject())).thenReturn(null);
		} catch (IncompatibileFileTypeException e) {
		}
		controller.setReport(mockReportProcessor);
		controller.processFile(someFile, new ModelMap());
		assertTrue(controller.hasErrors());
	}
	
	@Test
	public void test_process_failed_with_exception() {
		controller = new ReportValidationController();
		MockMultipartFile someFile = new MockMultipartFile("data", "filename.xml", "application/xml",
				"some xml".getBytes());
		
		try {
			Mockito.when(mockReportProcessor.getInvalidTransactions(Mockito.anyObject())).thenThrow(new IncompatibileFileTypeException());
		} catch (IncompatibileFileTypeException e) {
		}
		controller.setReport(mockReportProcessor);
		controller.processFile(someFile, new ModelMap());
		assertTrue(controller.hasErrors());
	}
	
}
